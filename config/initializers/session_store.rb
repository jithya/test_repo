# Be sure to restart your server when you modify this file.

# Your secret key for verifying cookie session data integrity.
# If you change this key, all old sessions will become invalid!
# Make sure the secret is at least 30 characters and all random, 
# no regular words or you'll be exposed to dictionary attacks.
ActionController::Base.session = {
  :key         => '_test_git_session',
  :secret      => '92ea1551414b6ce127ebaf45463dc8f3db092e853c7760fc3f40358abd99ca6a206657ea0971acdbb15db6fb559d92d3f8e6786155b8e77764e893714d73fcfe'
}

# Use the database for sessions instead of the cookie-based default,
# which shouldn't be used to store highly confidential information
# (create the session table with "rake db:sessions:create")
# ActionController::Base.session_store = :active_record_store
